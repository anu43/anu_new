import { sveltekit } from '@sveltejs/kit/vite';
import { enhancedImages } from '@sveltejs/enhanced-img';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [sveltekit()],
	assetsInclude: ['*.webm', '*.mp4'],
	server: {
		fs: {
			allow: ['..']
		}
	},
	build: {
		assetsInlineLimit: 0, // disable inlining assets
	}

});
