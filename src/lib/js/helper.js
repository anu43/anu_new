// ---------
// CONSTANTS
// ---------

export const mobileWidthThreshold = 1100;

// -----------------------
// GLOBAL HELPER FUNCTIONS 
// -----------------------
export function browserCanPlayH265(vidEl) {
  return vidEl.canPlayType('video/mp4; codecs=hvc1') === "probably";
}

export function importAll(r) {
  let files = {};
  r.keys().map(item => { files[item.replace('./', '')] = r(item); });
  return files;
}

export function importComponentMedia(product, component) {

  let mediaFiles = {};

  switch (product) {
    case 'InannaN60':
      if (component === "carousel") 
        mediaFiles = import.meta.glob('../img/InannaN60/carousel/*.(jpg|png|webp)', {query: '?url', eager: true});
      else if (component === "infinite-carousel")
        mediaFiles = import.meta.glob('../img/InannaN60/infinite-carousel/*.(jpg|png|webp)', {query: '?url', eager: true});
      else if (component === "scroll-sequence")
        mediaFiles = import.meta.glob('../img/InannaN60/scroll-sequence/*.(jpg|png|webp)', {query: '?url', eager: true});
      else if (component === "teaser" || component === "trailer")
        mediaFiles = import.meta.glob('../vid/InannaN60/teaser.(mp4|webm)', {query: '?url', eager: true});
      else if (component === "feature-scroller")
        mediaFiles = import.meta.glob('../img/InannaN60/feature-scroller/*.(jpg|png|webp)', {query: '?url', eager: true});
      else
        mediaFiles = import.meta.glob('../img/InannaN60/*.(jpg|png|webp)', {query: '?url', eager: true});
      break;
    default:
      if (component === "loading-spinner")
        mediaFiles = import.meta.glob('../img/loading-spinner/*.(jpg|png|webp)', {query: '?url', eager: true});
      else if (component === "scroll-indicator") 
        mediaFiles = import.meta.glob('../img/scroll-indicator/*.(jpg|png|webp)', {query: '?url', eager: true});
      else
        mediaFiles = import.meta.glob('../img/InannaN60/*.(jpg|png|webp)', {query: '?url', eager: true});
    break;
  }
  let fileArr = [];

  Object.entries(mediaFiles).forEach(element => {
    fileArr.push(element[1].src || element[1].default);
  });

  fileArr.sort(function(a, b) {
    const aFileName = a.split('/').pop();
    const bFileName = b.split('/').pop();

    const aNumber = parseInt(aFileName.split('.')[0], 10);
    const bNumber = parseInt(bFileName.split('.')[0], 10);

    return aNumber - bNumber;
  });

  return fileArr;
}

export function removeClass(query, className) {
  document.querySelectorAll(query).forEach((element) => {
    if (element.classList.contains(className))
      element.classList.remove(className);
  });
}

export function addClass(query, className) {
  document.querySelectorAll(query).forEach((element) => {
    if (!element.classList.contains(className))
      element.classList.add(className);
  });
}

export function hasClass(query, className) {
  return Array.from(document.querySelectorAll(query)).some(({classList}) => classList.contains(className));
}

export function elHasClass(el, className) {
  return el.classList.contains(className);
}

export const elementIsInViewport = (el, partiallyVisible = false) => {
  const { top, left, bottom, right } = el.getBoundingClientRect();
  const { innerHeight, innerWidth } = window;
  return partiallyVisible
    ? ((top > 0 && top < innerHeight) ||
        (bottom > 0 && bottom < innerHeight)) &&
        ((left > 0 && left < innerWidth) || (right > 0 && right < innerWidth))
    : top >= 0 && left >= 0 && bottom <= innerHeight && right <= innerWidth;
}

export function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

export function getRemInPx(rem) {
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

export function getEmInPx(em, element) {
  return em * parseFloat(getComputedStyle(element).fontSize);
}

export function getCoords(elem) {
  var box = elem.getBoundingClientRect();

  var body = document.body;
  var docEl = document.documentElement;

  var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
  var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

  var clientTop = docEl.clientTop || body.clientTop || 0;
  var clientLeft = docEl.clientLeft || body.clientLeft || 0;

  var top  = box.top +  scrollTop - clientTop;
  var left = box.left + scrollLeft - clientLeft;

  return { top: Math.round(top), left: Math.round(left) };
}

export const easings = {
  // easeOutSine:    {name: 'easeOutSine', thumb: '/img/easings/easeOutSine.svg', func: (t) => { return Math.sin((t * Math.PI) / 2) }},
  // easeOutCubic:   {name: 'easeOutCubic', thumb: '/img/easings/easeOutCubic.svg', func: (t) => { return 1 - Math.pow(1 - t, 3) }},
  easeOutQuint:   {name: 'easeOutQuint', thumb: '/img/easings/easeOutQuint.svg', func: (t) => { return 1 - Math.pow(1 - t, 5) }}
  // easeOutCirc:    {name: 'easeOutCirc', thumb: '/img/easings/easeOutCirc.svg', func: (t) => { return Math.sqrt(1 - Math.pow(t - 1, 2)) }},
  // easeOutElastic: {name: 'easeOutElastic', thumb: '/img/easings/easeOutElastic.svg', func: (t) => {
  //   const c4 = (2 * Math.PI) / 3;
  //   return t === 0
  //   ? 0
  //   : t === 1
  //   ? 1
  //   : Math.pow(2, -20 * t) * Math.sin((t * 5 - 0.7) * c4) + 1;
  // }},
  // easeOutQuad:    {name: 'easeOutQuad', thumb: '/img/easings/easeOutQuad.svg', func: (t) => { return 1 - Math.pow(1 - t, 2) }},
  // easeOutQuart:   {name: 'easeOutQuart', thumb: '/img/easings/easeOutQuart.svg', func: (t) => { return 1 - Math.pow(1 - t, 4) }},
  // easeOutExpo:    {name: 'easeOutExpo', thumb: '/img/easings/easeOutExpo.svg', func: (t) => { return t === 1 ? 1 : 1 - Math.pow(2, -10 * t) }},
  // easeOutBack:    {name: 'easeOutBack', thumb: '/img/easings/easeOutBack.svg', func: (t) => { const c1 = 1.70158; const c3 = c1 + 1; return 1 + c3 * Math.pow(t - 1, 3) + c1 * Math.pow(t - 1, 2); }},
  // easeOutBounce:  {name: 'easeOutBounce', thumb: '/img/easings/easeOutBounce.svg', func: (t) => { const n1 = 7.5625; const d1 = 2.75;
  //   if (t < 1 / d1)
  //     return n1 * t * t;
  //   else if (t < 2 / d1) 
  //     return n1 * (t -= 1.5 / d1) * t + 0.75;
  //   else if (t < 2.5 / d1) 
  //     return n1 * (t -= 2.25 / d1) * t + 0.9375;
  //   else
  //     return n1 * (t -= 2.625 / d1) * t + 0.984375;
  // }},
  // easeOutTen:     {name: 'easeOutTen', thumb: '', func: (t) => { return 1.4 - Math.pow(1 - t, 14) }}
}