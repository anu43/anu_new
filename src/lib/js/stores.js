import { writable } from 'svelte/store';
import { easings } from '$lib/js/helper.js';

export const STATE = {
  Loading:    "loading",    // If the page is loading
  Home:       "home",       // If the home page is shown
  Contact:    "contact",    // If the contact page is shown
  About:      "about",      // If the about page is shown
  Product:    "product",    // If the product page is shown
};

export const lenis = writable();

export const currentScrollEasing = writable(easings.easeOutQuint);

export const state = writable(STATE.Loading);
export const prevState = writable(STATE.Loading);

export const darkMode = writable(false);

export const homeSlideLoaded = writable(false);