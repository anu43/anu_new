// svelte.config.js
import adapter from '@sveltejs/adapter-static';
import { optimizeDeps } from 'vite';

export default {
  kit: {
    adapter: adapter({
      pages: 'public',
      assets: 'public',
      fallback: undefined,
      precompress: false,
      strict: true
    })
    // paths: {
    //   base: process.env.NODE_ENV === "production" ? "/anu_new" : ""
    // },
  }
};